const use_Proxy = true; //Flickr API only supports JSONP, and fetch() doesn't.
const defaultLoc = { latitude: 48.8575, longitude: 2.2982 }
const apiKey = "889c458799a16a30aaeda013e7ec2a63&"
let latitude = ""
let longitude = ""
let photoArray = []
let activePhotoIndex = 0
let photoResultsPageIndex = 1
async function makeFetch() {
    let url = searchURL()
    const response = await fetch(url)
    const data = await response.json()
    photoArray = data.photos.photo
    console.dir(photoArray)

    // const title = data.photos.photo.title
}
async function nextFetch() {
    let url = nextPage()
    const response = await fetch(url)
    const data = await response.json()
    photoArray = data.photos.photo
        // let title = data.photos.photo.title
}

function nextPage() {
    photoResultsPageIndex++
    makeFetch();
    displayPhoto();
}

function buildImageSrcURL() {
    return `https://farm${photoArray[activePhotoIndex].farm}.staticflickr.com/${photoArray[activePhotoIndex].server}/${photoArray[activePhotoIndex].id}_${photoArray[activePhotoIndex].secret}.jpg`
}

function searchURL() {
    const proxy = "https://shrouded-mountain-15003.herokuapp.com/"

    return (use_Proxy ? proxy : ``) +
        `https://flickr.com/services/rest/?` +
        `api_key=889c458799a16a30aaeda013e7ec2a63&` +
        `format=json&` +
        `nojsoncallback=1&` +
        `method=flickr.photos.search&` +
        `safe_search=1&` +
        `per_page=5&` +
        `page=${photoResultsPageIndex}&` +
        `lat=${latitude}&` +
        `lon=${longitude}&` +
        `text=People`
};

function geoFindMe() {
    function success(position) {
        longitude = position.coords.longitude;
        latitude = position.coords.latitude;
        makeFetch();
    }

    function error() {
        latitude = defaultLoc.latitude
        longitude = defaultLoc.longitude
        makeFetch();
    }
    if (!navigator.geolocation) {
        textContent = 'Geolocation is not supported by your browser';
    } else {
        textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error);
    }
}
geoFindMe();



function displayPhoto() {
    let selectDiv = document.getElementById("photoWindow")
    let urlDiv = document.getElementById('url')
    urlDiv.innerHTML = ''
    selectDiv.innerHTML = ""
    let makePhoto = document.createElement("img")
    makePhoto.src = buildImageSrcURL();
    console.log(buildImageSrcURL())
    selectDiv.appendChild(makePhoto)
    let showUrl = document.getElementById("url")
    let displayOnPage = document.createElement('span')
    let photoPageUrl = `https://flickr.com/photos/${photoArray[0].owner}/${photoArray[0].id}`
    showUrl.displayOnPage
    urlDiv.appendChild(displayOnPage)
    displayOnPage.innerHTML = photoPageUrl
}

function nextPhoto() {
    if (activePhotoIndex <= 3) {
        activePhotoIndex++;
    } else {
        activePhotoIndex = 0;
    }
    displayPhoto();
}

function previousPhoto() {
    if (activePhotoIndex >= 1) {
        activePhotoIndex--;
    } else {
        activePhotoIndex = 4;
    }
    displayPhoto();
}